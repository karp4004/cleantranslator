package karp4004.rxtranslator.di;

import dagger.Module;

/**
 * @author Karpov Oleg
 */

@Module(subcomponents = MainActivityComponent.class)
public class ApplicationModule {
}