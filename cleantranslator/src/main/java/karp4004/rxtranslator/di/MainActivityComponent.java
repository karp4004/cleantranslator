package karp4004.rxtranslator.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import karp4004.rxtranslator.presentation.view.MainActivity;

/**
 * @author Karpov Oleg
 */

@Subcomponent(modules = MainActivityModule.class)
public interface MainActivityComponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {
        public abstract Builder mainActivityModule(MainActivityModule myActivityModule);

        @Override
        public void seedInstance(MainActivity instance) {
            mainActivityModule(new MainActivityModule(instance));
        }

        public abstract MainActivityComponent build();
    }
}
