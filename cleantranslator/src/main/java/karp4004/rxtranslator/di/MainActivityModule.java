package karp4004.rxtranslator.di;

import dagger.Module;
import dagger.Provides;
import karp4004.rxtranslator.data.repository.LocalHistoryRepository;
import karp4004.rxtranslator.data.repository.TranslateRepository;
import karp4004.rxtranslator.data.repository.YandexTranslateRepository;
import karp4004.rxtranslator.domain.converter.LocalHistoryConverter;
import karp4004.rxtranslator.domain.converter.TranslateRequestConverter;
import karp4004.rxtranslator.domain.converter.TranslateResponseConverter;
import karp4004.rxtranslator.domain.converter.TranslateResponseConverterImpl;
import karp4004.rxtranslator.domain.interactor.LocalHistoryInteractor;
import karp4004.rxtranslator.domain.interactor.TranslateInteractor;
import karp4004.rxtranslator.presentation.presenter.TranslatePresenter;
import karp4004.rxtranslator.presentation.view.ITranslateView;
import karp4004.rxtranslator.presentation.view.MainActivity;

/**
 * @author Karpov Oleg
 */

@Module
public class MainActivityModule {

    private MainActivity mainActivity;

    public MainActivityModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    ITranslateView provideMainView() {
        return mainActivity;
    }

    @Provides
    TranslateRepository provideTranslateRepository() {
        return new YandexTranslateRepository();
    }

    @Provides
    TranslateResponseConverter provideTranslateConverter() {
        return new TranslateResponseConverterImpl();
    }

    @Provides
    TranslateInteractor provideTranslateInteractor(TranslateRepository translateRepository,
                                                   TranslateResponseConverter translateConverter) {
        return new TranslateInteractor(translateRepository, translateConverter);
    }

    @Provides
    LocalHistoryRepository provideLocalHistoryRepository() {
        return new LocalHistoryRepository(mainActivity.getApplicationContext());
    }

    @Provides
    TranslateRequestConverter provideLanguageConverter() {
        return new TranslateRequestConverter();
    }

    @Provides
    LocalHistoryConverter provideLocalHistoryConverter() {
        return new LocalHistoryConverter();
    }

    @Provides
    LocalHistoryInteractor provideLocalHistoryInteractor(LocalHistoryRepository localHistoryRepository,
                                                         LocalHistoryConverter localHistoryConverter) {
        return new LocalHistoryInteractor(localHistoryRepository, localHistoryConverter);
    }

    @Provides
    TranslatePresenter provideMainPresenter(ITranslateView translateView,
                                            TranslateInteractor translateInteractor,
                                            LocalHistoryInteractor localHistoryInteractor,
                                            TranslateRequestConverter languageConverter) {
        return new TranslatePresenter(translateView,
                translateInteractor,
                localHistoryInteractor,
                languageConverter);
    }
}
