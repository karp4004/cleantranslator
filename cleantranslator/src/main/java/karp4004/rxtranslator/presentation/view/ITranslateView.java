package karp4004.rxtranslator.presentation.view;

import karp4004.rxtranslator.presentation.model.LanguageListModel;

/**
 * @author Karpov Oleg
 */

public interface ITranslateView {
    void setLanguages(LanguageListModel languages);
    void setTranslateResult(String result);

    void setRequestHistory(String[] result);
}
