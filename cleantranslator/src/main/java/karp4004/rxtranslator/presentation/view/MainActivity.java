package karp4004.rxtranslator.presentation.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.jakewharton.rxbinding2.widget.RxAdapterView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import karp4004.rxtranslator.R;
import karp4004.rxtranslator.domain.interactor.LocalHistoryInteractor;
import karp4004.rxtranslator.domain.interactor.TranslateInteractor;
import karp4004.rxtranslator.presentation.adapter.LanguageAdapter;
import karp4004.rxtranslator.presentation.model.LanguageListModel;
import karp4004.rxtranslator.presentation.presenter.TranslatePresenter;

public class MainActivity extends AppCompatActivity implements ITranslateView {

    @BindView(R.id.translateButton)
    View translateButton;
    @BindView(R.id.translateResult)
    TextView translateResult;
    @BindView(R.id.translateText)
    AutoCompleteTextView translateText;
    @BindView(R.id.langSpinner1)
    Spinner langSpinner1;
    @BindView(R.id.langSpinner2)
    Spinner langSpinner2;

    @Inject
    TranslateInteractor translateInteractor;
    @Inject
    LocalHistoryInteractor localHistoryInteractor;
    @Inject
    TranslatePresenter mTranslatePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        AndroidInjection.inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mTranslatePresenter.onViewCreated(RxAdapterView.itemSelections(langSpinner1), RxAdapterView.itemSelections(langSpinner2), RxTextView.textChangeEvents(translateText));
    }

    @Override
    public void setLanguages(LanguageListModel languages) {
        langSpinner1.setAdapter(new LanguageAdapter(languages, this));
        langSpinner2.setAdapter(new LanguageAdapter(languages, this));
    }

    @OnClick(R.id.translateButton)
    public void translateButtonClick(Button button) {
        mTranslatePresenter.translate(translateText.getText().toString());
    }

    @Override
    public void setTranslateResult(String translateResult) {
        this.translateResult.setText(translateResult);
    }

    @Override
    public void setRequestHistory(String[] result) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, result);
        translateText.setAdapter(adapter);
    }
}

