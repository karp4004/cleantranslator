package karp4004.rxtranslator.presentation.model;

/**
 * @author Karpov Oleg
 */

public class LanguageModel {

    private String languageName;
    private String languageShortName;

    public LanguageModel(String languageName, String languageShortName) {
        this.languageName = languageName;
        this.languageShortName = languageShortName;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageShortName() {
        return languageShortName;
    }

    public void setLanguageShortName(String languageShortName) {
        this.languageShortName = languageShortName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LanguageModel that = (LanguageModel) o;

        if (languageName != null ? !languageName.equals(that.languageName) : that.languageName != null) {
            return false;
        }
        return languageShortName != null ? languageShortName.equals(that.languageShortName) : that.languageShortName == null;
    }

    @Override
    public int hashCode() {
        int result = languageName != null ? languageName.hashCode() : 0;
        result = 31 * result + (languageShortName != null ? languageShortName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LanguageModel{");
        sb.append("languageName='").append(languageName).append('\'');
        sb.append(", languageShortName='").append(languageShortName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
