package karp4004.rxtranslator.presentation.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Karpov Oleg
 */

public class LanguageListModel {

    public static final String EMPTY_STRING = "";

    private List<LanguageModel> mLanguageModelList = new ArrayList<LanguageModel>() {
        {
            add(new LanguageModel("Russian", "ru"));
            add(new LanguageModel("English", "en"));
            add(new LanguageModel("Deuch", "de"));
        }
    };

    public String getShortName(int index) throws IndexOutOfBoundsException {
        return getLanguageModel(index).getLanguageShortName();
    }

    public String getName(int index) throws IndexOutOfBoundsException {
        return getLanguageModel(index).getLanguageName();
    }

    public int getCount() {
        return mLanguageModelList.size();
    }

    public LanguageModel getLanguageModel(int index) throws IndexOutOfBoundsException {
        if (index >= 0 && index <= mLanguageModelList.size()) {
            return mLanguageModelList.get(index);
        }

        throw new IndexOutOfBoundsException("" + index);
    }
}
