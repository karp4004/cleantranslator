package karp4004.rxtranslator.presentation.presenter;

import com.jakewharton.rxbinding2.InitialValueObservable;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import karp4004.rxtranslator.data.bean.RequestHistoryBean;
import karp4004.rxtranslator.data.bean.TranslateRequestBean;
import karp4004.rxtranslator.domain.converter.TranslateRequestConverter;
import karp4004.rxtranslator.domain.interactor.LocalHistoryInteractor;
import karp4004.rxtranslator.domain.interactor.TranslateInteractor;
import karp4004.rxtranslator.domain.model.TranslateResponse;
import karp4004.rxtranslator.presentation.model.LanguageListModel;
import karp4004.rxtranslator.presentation.view.ITranslateView;

import static karp4004.rxtranslator.presentation.model.LanguageListModel.EMPTY_STRING;

/**
 * @author Karpov Oleg
 */

public class TranslatePresenter {

    private static final int TEXT_CHANGE_DELAY = 1000;

    private ITranslateView mTranslateView;
    private TranslateInteractor mTranslateInteractor;
    private LocalHistoryInteractor mLocalHistoryInteractor;
    private TranslateRequestConverter mTranslateRequestConverter;
    private LanguageListModel mLanguageModelList = new LanguageListModel();

    private String[] historyModel = new String[]{EMPTY_STRING};
    private TranslateRequestBean mTranslateRequestBean = new TranslateRequestBean(EMPTY_STRING, EMPTY_STRING);

    public TranslatePresenter(ITranslateView translateView,
                              TranslateInteractor translateInteractor,
                              LocalHistoryInteractor localHistoryInteractor,
                              TranslateRequestConverter languageConverter) {
        mTranslateView = translateView;
        mTranslateInteractor = translateInteractor;
        mLocalHistoryInteractor = localHistoryInteractor;
        mTranslateRequestConverter = languageConverter;
    }

    public void onViewCreated(InitialValueObservable<Integer> langFrom,
                              InitialValueObservable<Integer> langTo,
                              InitialValueObservable<TextViewTextChangeEvent> textObservable) {
        mTranslateView.setLanguages(mLanguageModelList);
        mLocalHistoryInteractor.getHistory(historyObserver);

        Observable.combineLatest(langFrom.map(integer -> mLanguageModelList.getShortName(integer)),
                langTo.map(integer -> mLanguageModelList.getShortName(integer)),
                textObservable,
                (s1, s2, s3) -> mTranslateRequestConverter.convert(s1, s2, s3.text().toString()))
                .subscribe(s -> {
                    mTranslateRequestBean = s;
                    mTranslateInteractor.translate(mTranslateRequestBean, translateObserver);
                });

        textObservable.delay(TEXT_CHANGE_DELAY, TimeUnit.MILLISECONDS).subscribe(text -> translate(text.text().toString()));
    }

    public void translate(String translateText) {
        mTranslateRequestBean.setText(translateText);
        mTranslateInteractor.translate(mTranslateRequestBean, translateObserver);
    }

    private boolean hasHistoryItem(String item) {
        for (String history :
                historyModel) {
            if (item.equals(history)) {
                return true;
            }
        }

        return false;
    }

    private Observer<TranslateResponse> translateObserver = new Observer<TranslateResponse>() {
        @Override
        public void onSubscribe(Disposable d) {
        }

        @Override
        public void onNext(final TranslateResponse value) {
            mTranslateView.setTranslateResult(value.getTranslatedText());
            mLocalHistoryInteractor.insertHistory(new RequestHistoryBean(value.getTranslateText()));

            if (!hasHistoryItem(value.getTranslateText())) {
                historyModel = Arrays.copyOf(historyModel, historyModel.length + 1);
                historyModel[historyModel.length - 1] = value.getTranslateText();
                mTranslateView.setRequestHistory(historyModel);
            }
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onComplete() {
        }
    };

    private SingleObserver<String[]> historyObserver = new SingleObserver<String[]>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(String[] strings) {
            historyModel = strings;
            mTranslateView.setRequestHistory(historyModel);
        }

        @Override
        public void onError(Throwable e) {

        }
    };
}
