package karp4004.rxtranslator.presentation.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import karp4004.rxtranslator.R;
import karp4004.rxtranslator.presentation.model.LanguageListModel;

/**
 * @author Karpov Oleg
 */

public class LanguageAdapter implements SpinnerAdapter {

    private LanguageListModel languages;
    private LayoutInflater layoutInflater;

    public LanguageAdapter(LanguageListModel languages, Context context) {
        this.languages = languages;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.language_item, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.languageName);
        textView.setText(languages.getName(position));

        return convertView;
    }

    @Override
    public int getCount() {
        return languages.getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.language_item, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.languageName);
        textView.setText(languages.getName(position));

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return languages.getLanguageModel(position);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
