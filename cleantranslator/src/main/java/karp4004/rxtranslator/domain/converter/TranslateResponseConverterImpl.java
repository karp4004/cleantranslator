package karp4004.rxtranslator.domain.converter;

import karp4004.rxtranslator.data.bean.TranslateRequestBean;
import karp4004.rxtranslator.data.bean.TranslateResponseBean;
import karp4004.rxtranslator.domain.model.TranslateResponse;

/**
 * @author Karpov Oleg
 */

public class TranslateResponseConverterImpl implements TranslateResponseConverter<TranslateRequestBean, TranslateResponseBean> {

    @Override
    public TranslateResponse convert(TranslateRequestBean requset, TranslateResponseBean response) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String str :
                response.getText()) {
            stringBuilder.append(str);
        }

        return new TranslateResponse(requset.getText(), stringBuilder.toString());
    }
}
