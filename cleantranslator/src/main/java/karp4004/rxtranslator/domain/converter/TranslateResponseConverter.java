package karp4004.rxtranslator.domain.converter;

import karp4004.rxtranslator.domain.model.TranslateResponse;

/**
 * @author Karpov Oleg
 */

public interface TranslateResponseConverter<REQUEST, RESPONSE> {
    TranslateResponse convert(REQUEST requset, RESPONSE reqponse);
}
