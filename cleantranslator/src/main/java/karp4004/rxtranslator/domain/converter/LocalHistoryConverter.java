package karp4004.rxtranslator.domain.converter;

import java.util.List;

import karp4004.rxtranslator.data.bean.RequestHistoryBean;

/**
 * @author Karpov Oleg
 */

public class LocalHistoryConverter {
    public String[] convert(List<RequestHistoryBean> requestHistoryBeanList) {
        String[] strings = new String[requestHistoryBeanList.size()];
        for (int i = 0; i < requestHistoryBeanList.size(); i++) {
            strings[i] = requestHistoryBeanList.get(i).getHistoryItem();
        }

        return strings;
    }
}
