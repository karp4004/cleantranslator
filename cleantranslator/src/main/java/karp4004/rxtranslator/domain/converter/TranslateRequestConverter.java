package karp4004.rxtranslator.domain.converter;

import karp4004.rxtranslator.data.bean.TranslateRequestBean;

/**
 * @author Karpov Oleg
 */

public class TranslateRequestConverter {

    private static final String DELIMITER = "-";

    public TranslateRequestBean convert(String from, String to, String text) {
        return new TranslateRequestBean(from + DELIMITER + to, text);
    }
}
