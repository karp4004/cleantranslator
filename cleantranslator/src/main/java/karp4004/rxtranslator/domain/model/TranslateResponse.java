package karp4004.rxtranslator.domain.model;

/**
 * @author Karpov Oleg
 */

public class TranslateResponse {
    private String translateText;
    private String translatedText;

    public TranslateResponse(String translateText, String translatedText) {
        this.translateText = translateText;
        this.translatedText = translatedText;
    }

    public String getTranslateText() {
        return translateText;
    }

    public void setTranslateText(String translateText) {
        this.translateText = translateText;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TranslateResponse that = (TranslateResponse) o;

        if (translateText != null ? !translateText.equals(that.translateText) : that.translateText != null) {
            return false;
        }
        return translatedText != null ? translatedText.equals(that.translatedText) : that.translatedText == null;
    }

    @Override
    public int hashCode() {
        int result = translateText != null ? translateText.hashCode() : 0;
        result = 31 * result + (translatedText != null ? translatedText.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TranslateResponse{");
        sb.append("translateText='").append(translateText).append('\'');
        sb.append(", translatedText='").append(translatedText).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
