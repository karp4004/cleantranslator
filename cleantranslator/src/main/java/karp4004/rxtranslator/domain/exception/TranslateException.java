package karp4004.rxtranslator.domain.exception;

/**
 * @author Karpov Oleg
 */

public class TranslateException extends Exception {
    public TranslateException(String message) {
        super(message);
    }
}
