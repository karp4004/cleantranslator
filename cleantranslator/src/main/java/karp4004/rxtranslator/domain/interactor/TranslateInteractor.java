package karp4004.rxtranslator.domain.interactor;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import karp4004.rxtranslator.data.repository.TranslateRepository;
import karp4004.rxtranslator.domain.converter.TranslateResponseConverter;
import karp4004.rxtranslator.domain.exception.TranslateException;
import karp4004.rxtranslator.domain.model.TranslateResponse;

/**
 * @author Karpov Oleg
 */

public class TranslateInteractor<REQUEST, RESPONSE> {
    private Observable<TranslateResponse> mTranslateObservable;
    protected REQUEST mTranslateRequestBean;

    public TranslateInteractor(TranslateRepository translateRepository, TranslateResponseConverter translateConverter) {
        mTranslateObservable =
                Observable.create(subscriber -> {
                try {
                    RESPONSE response = (RESPONSE) translateRepository.translate(mTranslateRequestBean);
                    subscriber.onNext(response);
                    subscriber.onComplete();
                } catch (IOException e) {
                    subscriber.onError(new TranslateException(e.getMessage()));
                }
                })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .map(translateResponseBean -> translateConverter.convert(mTranslateRequestBean, translateResponseBean));
    }

    public void translate(REQUEST translateRequestBean, Observer observer) {
        mTranslateRequestBean = translateRequestBean;
        mTranslateObservable.subscribe(observer);
    }
}
