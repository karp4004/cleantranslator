package karp4004.rxtranslator.domain.interactor;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import karp4004.rxtranslator.data.bean.RequestHistoryBean;
import karp4004.rxtranslator.data.repository.LocalHistoryRepository;
import karp4004.rxtranslator.domain.converter.LocalHistoryConverter;

/**
 * @author Karpov Oleg
 */

public class LocalHistoryInteractor {

    private LocalHistoryRepository mLocalHistoryRepository;
    private LocalHistoryConverter mLocalHistoryConverter;

    public LocalHistoryInteractor(LocalHistoryRepository localHistoryRepository,
                                  LocalHistoryConverter localHistoryConverter) {
        mLocalHistoryRepository = localHistoryRepository;
        mLocalHistoryConverter = localHistoryConverter;
    }

    public void insertHistory(RequestHistoryBean... history) {
        Observable.create(e -> mLocalHistoryRepository.insertHistory(history))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    public void getHistory(SingleObserver single) {
        mLocalHistoryRepository
                .getHistory()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(history -> mLocalHistoryConverter.convert(history))
                .subscribe(single);
    }
}
