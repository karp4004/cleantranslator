package karp4004.rxtranslator.data.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;

import io.reactivex.Single;
import karp4004.rxtranslator.data.bean.RequestHistoryBean;

/**
 * @author Karpov Oleg
 */

public class LocalHistoryRepository {

    private static final String DATABASE_NAME = "local_history_database";

    private HistoryDatabase historyDatabase;

    public LocalHistoryRepository(Context context) {
        historyDatabase = Room.databaseBuilder(context,
                HistoryDatabase.class, DATABASE_NAME).build();
    }

    public void insertHistory(RequestHistoryBean... history) {
        historyDatabase.historyDao().insertHistory(history);
    }

    public Single<List<RequestHistoryBean>> getHistory() {
        return historyDatabase.historyDao().getHistory();
    }

    @Dao
    public interface HistoryDao {
        @Query("SELECT * FROM history")
        Single<List<RequestHistoryBean>> getHistory();

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertHistory(RequestHistoryBean... history);
    }

    @Database(entities = {RequestHistoryBean.class}, version = 1)
    public static abstract class HistoryDatabase extends RoomDatabase {
        public abstract HistoryDao historyDao();
    }
}
