package karp4004.rxtranslator.data.repository;

import java.io.IOException;

/**
 * @author Karpov Oleg
 */

public interface TranslateRepository<REQUEST, RESPONSE> {
    RESPONSE translate(REQUEST translateRequestBean) throws IOException;
}
