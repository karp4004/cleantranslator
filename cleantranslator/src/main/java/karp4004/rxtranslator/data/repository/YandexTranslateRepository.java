package karp4004.rxtranslator.data.repository;

import java.io.IOException;

import karp4004.rxtranslator.data.bean.TranslateRequestBean;
import karp4004.rxtranslator.data.bean.TranslateResponseBean;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * @author Karpov Oleg
 */

public class YandexTranslateRepository implements TranslateRepository<TranslateRequestBean, TranslateResponseBean> {

    private static final String mBaseUrl = "https://translate.yandex.net/";
    private static final String mHeaderAccept = "Accept: application/json";
    private static final String mResourceUrl = "api/v1.5/tr.json/translate";
    private static final String mQueryKey = "key";
    private static final String mQueryText = "text";
    private static final String mQueryLang = "lang";
    private static final String mQueryKeyValue = "trnsl.1.1.20171114T105954Z.65934f83cb06a079.af724bf58f0e4bf612ac2b422ba1f37964ea8a5a";

    private YandexTranslateApi mYandexTranslateApi;

    public YandexTranslateRepository() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        mYandexTranslateApi = retrofit.create(YandexTranslateApi.class);
    }

    @Override
    public TranslateResponseBean translate(TranslateRequestBean translateRequestBean) throws IOException {
        Call<TranslateResponseBean> call = mYandexTranslateApi.translate(mQueryKeyValue, translateRequestBean.getText(), translateRequestBean.getLanguage());
        Response<TranslateResponseBean> response = call.execute();
        return response.body();
    }

    public interface YandexTranslateApi {
        @Headers({
                mHeaderAccept
        })
        @GET(mResourceUrl)
        Call<TranslateResponseBean> translate(@Query(mQueryKey) String key, @Query(mQueryText) String text, @Query(mQueryLang) String lang);
    }
}
