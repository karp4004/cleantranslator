package karp4004.rxtranslator.data.bean;

/**
 * @author Karpov Oleg
 */

public class TranslateRequestBean {
    private String mLanguage;
    private String mText;

    public TranslateRequestBean(String language, String text) {
        mLanguage = language;
        mText = text;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TranslateRequestModel{");
        sb.append("mLanguage='").append(mLanguage).append('\'');
        sb.append(", mText='").append(mText).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TranslateRequestBean that = (TranslateRequestBean) o;

        if (mLanguage != null ? !mLanguage.equals(that.mLanguage) : that.mLanguage != null) {
            return false;
        }
        return mText != null ? mText.equals(that.mText) : that.mText == null;

    }

    @Override
    public int hashCode() {
        int result = mLanguage != null ? mLanguage.hashCode() : 0;
        result = 31 * result + (mText != null ? mText.hashCode() : 0);
        return result;
    }
}
