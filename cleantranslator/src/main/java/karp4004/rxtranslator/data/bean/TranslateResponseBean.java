package karp4004.rxtranslator.data.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

/**
 * @author Karpov Oleg
 */

public class TranslateResponseBean {
    @JsonProperty("code")
    private int mCode;

    @JsonProperty("lang")
    private String mLanguage;

    @JsonProperty("text")
    private String[] mText;

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        mCode = code;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public String[] getText() {
        return mText;
    }

    public void setText(String[] text) {
        mText = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TranslateResponseBean that = (TranslateResponseBean) o;

        if (mCode != that.mCode) {
            return false;
        }
        if (mLanguage != null ? !mLanguage.equals(that.mLanguage) : that.mLanguage != null) {
            return false;
        }
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(mText, that.mText);

    }

    @Override
    public int hashCode() {
        int result = mCode;
        result = 31 * result + (mLanguage != null ? mLanguage.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(mText);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TranslateResponseBean{");
        sb.append("mCode=").append(mCode);
        sb.append(", mLanguage='").append(mLanguage).append('\'');
        sb.append(", mText=").append(Arrays.toString(mText));
        sb.append('}');
        return sb.toString();
    }
}
