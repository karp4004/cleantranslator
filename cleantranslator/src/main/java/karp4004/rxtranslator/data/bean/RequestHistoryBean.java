package karp4004.rxtranslator.data.bean;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * @author Karpov Oleg
 */

@Entity(tableName = "history",
        indices = {@Index(value = {"history_item"}, unique = true)})
public class RequestHistoryBean {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "history_item")
    private String historyItem;

    public RequestHistoryBean(@NonNull String historyItem) {
        this.historyItem = historyItem;
    }

    @NonNull
    public String getHistoryItem() {
        return historyItem;
    }

    public void setHistoryItem(@NonNull String historyItem) {
        this.historyItem = historyItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestHistoryBean that = (RequestHistoryBean) o;

        if (id != that.id) {
            return false;
        }
        return historyItem != null ? historyItem.equals(that.historyItem) : that.historyItem == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (historyItem != null ? historyItem.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestHistoryBean{");
        sb.append("id=").append(id);
        sb.append(", historyItem='").append(historyItem).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
